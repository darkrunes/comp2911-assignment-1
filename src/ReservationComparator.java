import java.util.*;

/**
 * Class used for sorting lists of reservations
 * @author Saffat Shams Akanda,	z5061498
 */
public class ReservationComparator implements Comparator<Reservation> {

	/**
	 * Compares 2 reservations based on their starting dates.
	 * @param r1	The first reservation
	 * @param r2	The second reservation
	 * @return 		An integer that tells which number is larger	
	 */
	@Override
	public int compare(Reservation r1, Reservation r2) {
		Date s1 = r1.getStartDate().getTime();
		Date s2 = r2.getStartDate().getTime();
		
		if (s1.equals(s2)) return 0;
		
		return s1.before(s2) ? -1 : 1;
	}

}
