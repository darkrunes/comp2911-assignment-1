import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * This class handles the data and manipulation of reservations
 * @author Saffat Shams Akanda, z5061498
 */
public class Reservation{
	private Venue ownerVenue;
	private int id;
	private ArrayList<Room> roomList;
	private Calendar startDate;
	private Calendar endDate;
	private int daysStaying;
	
	/**
	 * Constructor for Reservation.
	 * @param v		The venue that is making the reservation
	 * @param id	ID number to be assigned to the reservation
	 * @param r		ArrayList of the rooms to be reserved
	 * @param s		A Calendar representing the date the reservation starts
	 * @param e		A Calendar representing the date the reservation ends
	 */
	public Reservation(Venue v, int id, ArrayList<Room> r, Calendar s, Calendar e) {
		ownerVenue = v;
		this.id = id;
		roomList = r;
		startDate = s;
		endDate = e;
		daysStaying = calculateDaysStay(s, e);
	}

	/**
	 * @return		The venue where the reservation takes place
	 */
	public Venue getOwnerVenue() {
		return ownerVenue;
	}
	
	/**
	 * <b>Only to be used for changing booking, reservations should
	 * 	not be recycled for new bookings</b> 
	 * @param v		Set the owner to another venue
	 */
	public void setOwnerVenue(Venue v) {
		ownerVenue = v;
	}

	/**
	 * @return		Get the ID assigned to reservation
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return		An ArrayList containing the rooms booked
	 */
	public ArrayList<Room> getRoomList() {
		return roomList;
	}
	
	/**
	 * <b>Only to be used for changing booking, reservations should not
	 * 	be recycled for new bookings</b>
	 * @param r		New list of rooms to change booking to
	 */
	public void setRoomList(ArrayList<Room> r) {
		roomList = r;
	}

	/**
	 * @return		A calendar containing the date the reservation starts
	 */
	public Calendar getStartDate() {
		return startDate;
	}

	/**
	 * @return		A calendar containing the date the reservation ends
	 */
	public Calendar getEndDate() {
		return endDate;
	}
	
	/**
	 *<b>Only to be used for changing booking, reservations should not
	 * 	be recycled for new bookings.</b>
	 * @param s
	 * @param e
	 */
	public void setDates(Calendar s, Calendar e) {
		startDate = s;
		endDate = e;
		
		daysStaying = calculateDaysStay(s, e);
	}

	/**
	 * This method returns a string containing all the rooms that have been
	 * 	reserved.
	 * @return		String of rooms reserved
	 */
	public String getRoomsString() {		
		// No need to process the rest if only 1 room
		if (roomList.size() == 1) {
			return roomList.get(0).getName();
		}
		
		StringBuilder sb = new StringBuilder();
		Room lastRoom = roomList.get(roomList.size() - 1);

		// Append each room to the string, adding spaces when needed
		for (Room r: roomList) {
			sb.append(r.getName());
			if (r != lastRoom) {
				sb.append(" ");
			}
		}
		
		return sb.toString();
	} 
	
	/**
	 * @return		A integer representing how many days the reservation lasts
	 */
	public int getDaysStaying() {
		return daysStaying;
	}
	
	/**
	 * This is called on construction and when changing of dates. The method calculates how many
	 *  days reservation lasts
	 * @param s		Calendar representing the date the booking starts
	 * @param e		Calendar representing the date the booking ends
	 * @return		An integer representing the number of days booked.
	 */
	private int calculateDaysStay(Calendar s, Calendar e) {
		//	The calendar stores times in milliseconds (epoch time)
		//  Add 1 to the result due to the following example
		//  e.g. S = 25 March, E = 25 March. Difference is 0 but the number of days booked is 1.
		return (int) TimeUnit.DAYS.convert(e.getTimeInMillis() - s.getTimeInMillis(), TimeUnit.MILLISECONDS) + 1;
	}
}
