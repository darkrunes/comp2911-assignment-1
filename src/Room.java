import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Handles the data and management of rooms
 * @author Saffat Shams Akanda, z5061498
 */
public class Room {
	private String name;
	private String size;
	private LinkedList<Reservation> reservationList;
	private Venue ownerVenue;
	
	/**
	 * Constructor
	 * @param name			Name of the room - String
	 * @param size			Size of the room - String
	 * @param ownerVenue	Venue that owns the room - Venue
	 */
	public Room(String name, String size, Venue ownerVenue) {
		this.name = name;
		this.size = size;
		this.ownerVenue = ownerVenue;
		
		reservationList = new LinkedList<Reservation>();
	}
	
	/**
	 * Checks weather the room is vacant during the given period.
	 * @param startDate		Calendar representing the start date
	 * @param endDate		Calendar representing the end date
	 * @return				A boolean indicating if the room is vacant
	 */
	public boolean isVacant (Calendar startDate, Calendar endDate) {
		Calendar d1;
		Calendar d2;
		
		for (Reservation r: reservationList) {
			d1 = r.getStartDate();
			d2 = r.getEndDate();
			
			/*
			 * If 		StartDate < D1 < EndDate or 
			 * 			StartDate < D2 < EndDate or
			 * 			D1 < StartDate < D2
			 * The proposed reservation clashes with another reservation in this room
			*/
			if ((d1.after(startDate) && d1.before(endDate)) || (d2.after(startDate) && d2.before(endDate))
					|| (startDate.after(d1) && startDate.before(d2))) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Adds a new reservation to the list, and sorts it
	 * @param res		Reservation to add
	 * @precondition	Reservation must be checked to be valid
	 * @postcondition	Valid reservation will be added to the list
	 */
	public void addReservation(Reservation res) {
		reservationList.add(res);
		updateList();
	}
	
	/**
	 * Cancels a given reservation, removing it from the rooms list
	 * @param res	Reservation to be cancelled
	 * @throws MissingReservationException		Exception will be thrown if reservation is not found
	 */
	public void cancelReservation(Reservation res) throws MissingReservationException {
		if (!reservationList.remove(res)) {
			throw new MissingReservationException("Reservation missing: " + res.getId() + " List: " + reservationList.toString());
		}
	}
	
	/**
	 * Returns a String containing the start of reservation dates and the days staying
	 * 	in ascending order.
	 * @return	String of starting dates of the reservations and num days it lasts
	 */
	public String returnFormattedReservations() {
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dateFormatter = new SimpleDateFormat(" MMM dd ");
		sb.append(name);	
		
		// Appends each reservation with the month's first letter capitalized
		for (Reservation r: reservationList) {
			// Gets the time of reservation in epoch time and converts it into a month
			sb.append(capitalizeFirstLetter(dateFormatter.format(r.getStartDate().getTime())));
			sb.append(r.getDaysStaying());
		}
		
		return sb.toString();
	}
	
	/**
	 * Converts a string array of sizes in the form
	 *  1 large 3 small
	 *  into an ArrayList of the form
	 *  large small small small
	 *  
	 * @param arr	Array of room sizes
	 * @return		ArrayList of accepted room sizes
	 */
	public static ArrayList<String> convertToList(String[] arr) {
		int numRoom;
		String roomSize;
		ArrayList<String> roomArr = new ArrayList<String>();
		
		// Gets the integer (j) and the size associated with it
		// Then in the second loop appends to the list that j times
		for (int i = 0; i < arr.length; i+= 2) {
			numRoom = Integer.parseInt(arr[i]);
			roomSize = arr[i+1];
			
			for (int j = 0; j < numRoom; j++) {
				roomArr.add(roomSize);
			}
		}
		
		
		return roomArr;
	}

	/**
	 * Sorts the list of reservations into ascending order of starting dates
	 */
	public void updateList() {
		Collections.sort(reservationList, new ReservationComparator());
	}
	
	/**
	 * @return	The name of the room
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return	The size of the room
	 */
	public String getSize() {
		return size;
	}

	/**
	 * @return	The list of reservations
	 */
	public LinkedList<Reservation> getReservationList() {
		return reservationList;
	}

	/**
	 * @return	The venue that owns the room
	 */
	public Venue getOwnerVenue() {
		return ownerVenue;
	}
	
	/**
	 * Capitalizes the first letter in a string
	 * @param s		String to be capitalized
	 * @return		Returns string with 1st letter capitalized
	 */
	private String capitalizeFirstLetter(String s) {
		// Capitalizes the first letter and appends the reset of the string to it
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}
}
