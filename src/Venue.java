import java.util.*;

/**
 * Manages the data and rooms for a venue
 * @author Saffat Shams Akanda, z5061498
 */
public class Venue {
	private ArrayList<Room> roomList;
	private String venueName;

	/**
	 * Constructor.
	 * @param name	Name of the venue
	 */
	public Venue (String name) {
		roomList = new ArrayList<Room>();
		venueName = name;
	}
	
	/**
	 * Makes a booking with the rooms available if possible otherwise returns null
	 * @param startDate		Calendar representing the day to start the booking
	 * @param endDate		Calendar representing the day to end the booking
	 * @param rooms			ArrayList containing the rooms required
	 * @param id			ID number assigned to request	
	 * @return				Returns a Reservation is booking is possible otherwise null
	 * @see Room#convertToList(String[])	Use Room.convertToList(String[]) to convert
	 * 										the given string into an acceptable ArrayList
	 */
	public Reservation makeBooking(Calendar startDate, Calendar endDate, ArrayList<String> rooms, int id)  {
		ArrayList<Room> roomsToReserve = new ArrayList<Room>();
		
		// Checks each room if it is vacant during the period
		// If so checks if a room of that size is required
		// Then removes it from the list of rooms required and adds it to 
		//   the list of rooms to book
		for (Room r: roomList) {
			if (r.isVacant(startDate, endDate)) {
				int sizeLoc = indexOfSize(r.getSize(), rooms);
				if (sizeLoc != -1) {
					roomsToReserve.add(r);
					rooms.remove(sizeLoc);
				}
			}
		}
		
		if (!rooms.isEmpty()) return null;
		
		Reservation newRes = new Reservation(this, id, roomsToReserve, startDate, endDate);
		
		for(Room r: roomsToReserve) {
			r.addReservation(newRes);
		}
		
		return newRes;
	}
	
	/**
	 * Cancels a given booking if it exists
	 * @param res		Reservation to be removed
	 * @precondition 	Reservation should exist
	 * @postcondition 	Reservation will be removed
	 */
	public void cancelBooking(Reservation res) {
		for(Room r: res.getRoomList()) {
			try {
				r.cancelReservation(res);
			}
			catch (MissingReservationException e) {
				e.printStackTrace();			// Should never happen
				System.out.println(e.getMessage());
				continue;
			}
		}
	}
	
	/**
	 * Checks if it is possible to change the booking and if so does it
	 * @param r				Reservation to be changed
	 * @param startDate		Calendar representing new start of reservation
	 * @param endDate		Calendar representing new end of reservation
	 * @param rooms			ArrayList of room sizes wanted
	 * @return				Boolean indicating weather or not reservation was successfully changed
	 * @throws MissingReservationException	If reservation does not exist in rooms list, but in hire system
	 * @see Room#convertToList(String[])	Use Room.convertToList(String[]) to convert
	 * 										the given string into an acceptable ArrayList
	 */
	public boolean changeBooking(Reservation r, Calendar startDate, Calendar endDate, ArrayList<String> rooms) throws MissingReservationException {
		
		ArrayList<String> roomsCopy = new ArrayList<String>();
		ArrayList<Room> potentialRooms = new ArrayList<Room>();
		String newRoom;
		
		// Make copy of rooms Array to modify
		for (String s: rooms) {
			newRoom = s;
			roomsCopy.add(newRoom);
		}
		
		// If the original venue is this venue, see if old rooms can be reused
		if (this == r.getOwnerVenue()) {
			for (Room oldRoom: r.getRoomList()) {
				int sizeLoc = indexOfSize(oldRoom.getSize(), roomsCopy);
				if (sizeLoc != -1) {
					potentialRooms.add(oldRoom);
					roomsCopy.remove(sizeLoc);
				}
			}
		}
		
		// Check to see if you can satisfy the rooms needed
		for (Room potentialRoom : roomList) {
			if (!potentialRooms.contains(potentialRoom) && potentialRoom.isVacant(startDate, endDate)) {
				int sizeLoc = indexOfSize(potentialRoom.getSize(), roomsCopy);
				if (sizeLoc != -1) {
					potentialRooms.add(potentialRoom);
					roomsCopy.remove(sizeLoc);
				}
			}
		}
		
		if (!roomsCopy.isEmpty()) return false;
		
		// If booking can be changed cancel the booking at the other venue, if not at this venue
		if (r.getOwnerVenue() != this) {
			r.getOwnerVenue().cancelBooking(r);
		} 
		else {
			// Cancel bookings from this venue if used
			for (Room oldRoom: r.getRoomList()) {
				oldRoom.cancelReservation(r);
			}
		}
		
		r.setOwnerVenue(this);
		r.setRoomList(potentialRooms);
		r.setDates(startDate, endDate);
		
		for (Room confirmedRoom: potentialRooms) {
			confirmedRoom.addReservation(r);
		}
		
		return true;
	}
	
	/**
	 * Prints out the rooms in the required format e.g.
	 * <p>
	 * Zoo Penguin Mar 15 Dec 31
	 */
	public void printBookings() {
		for (Room r: roomList) {
			System.out.printf("%s %s\n", venueName, r.returnFormattedReservations());
		}
	}
	
	/**
	 * Adds a room to the venue
	 * @param name		String name of new room
	 * @param size		String size of new room
	 */
	public void addRoom(String name, String size) {
		roomList.add(new Room(name, size, this));
	}
	
	/**
	 * Finds the first occurrence of a size in a list of room sizes and returns its index
	 * @param size			String size to search for
	 * @param roomSizes		ArrayList of String to search through
	 * @return				Returns index of first occurrence if found otherwise -1
	 */
	private int indexOfSize(String size, ArrayList<String> roomSizes) {
		for (int i = 0; i < roomSizes.size(); i++) {
			if (roomSizes.get(i).equals(size)) return i;
		}
		
		return -1;
	}
	
	/**
	 * @return		The venue's name
	 */
	public String getVenueName() {
		return venueName;
	}
}
