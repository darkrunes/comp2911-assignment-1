import java.io.*;
import java.util.*;

/**
 * This class handles user input and initialization of the system.
 * @author Saffat Shams Akanda,		z5061498
 */
public class VenueHireSystem {
	private LinkedList<Venue> venueList;
	private LinkedList<Reservation> reservationList;
	
	
	/**
	 * This is the main method that starts the programs
	 * 
	 * @param args		Arguments to be supplied from the command line to the program,
	 * 						To use the program the name of the input file has to be given.
	 */
	public static void main(String[] args) {
		VenueHireSystem vh = new VenueHireSystem();
		vh.readInput(args[0]);
	}
	
	/**
	 * Constructor for the venue hire system, initialize its collection variables
	 * no parameters needed.
	 */
	public VenueHireSystem() {
		venueList = new LinkedList<Venue>();
		reservationList = new LinkedList<Reservation>();
	}
	
	/**
	 * This method handles reading through the given file, splitting it and passing it
	 *  to another method to handle the different inputs.
	 * @param arg 		String name of the file to be read.
	 * @precondition 	Must be a valid file that can be read.
	 */
	private void readInput(String arg) {
	      Scanner sc = null;
	      String[] str;
	      try {
	          sc = new Scanner(new FileReader(arg));          
	      }
	      catch (FileNotFoundException|ArrayIndexOutOfBoundsException e) {
	    	  System.out.println("Enter the name of the file as an argument.");
	    	  System.exit(1);
	      } 
	      
	      while(sc.hasNextLine()) {
	    	  str = sc.nextLine().split(" ");
	    	  this.performVenueOperation(str);
	      }
	      
	      
	      
          if (sc != null) sc.close();
	}
	
	/**
	 * This method distinguishes between the commands given and performs the actions required.
	 * 	e.g. Making a request/booking and handles the majority of std output.
	 * 
	 * @param s		String Array of the line of the current line of input
	 * @see Room#convertToList(String[])	Room.convertToList, Used frequently for bookings and changes	
	 */
	private void performVenueOperation(String[] s) {
		Reservation r = null;
		Venue v;
		Calendar startDate;
		Calendar endDate;

		switch (s[0]) {
			case "Venue":
				boolean venueExist = false;
				
				// Loops through the current venues to check if the request venue exists
				// If the venue doesn't exist it is created and has the room added otherwise
				// The room is added to the appropriate venue
				for (Venue ven: venueList) {
					if (ven.getVenueName().equals(s[1])) {
						venueExist = true;
						ven.addRoom(s[2], s[3]);
						break;
					}
				}
				
				if (!venueExist) {
					v = new Venue(s[1]);
					venueList.add(v);
					v.addRoom(s[2], s[3]);
				}
				
				break;
				
			case "Request":
				startDate = new GregorianCalendar();
				endDate = new GregorianCalendar();	
				
				// s3 and s5 will always be integers in the given format if the the operation is "request"
				startDate.set(Calendar.YEAR, monthToInt(s[2]), Integer.parseInt(s[3]), 0, 0, 0);
				
				
				// If statement to check if the reservation lasts longer then a year
				if (isNextYear(s[2], s[3], s[4], s[5])) {
					endDate.set(Calendar.YEAR + 1, monthToInt(s[4]), Integer.parseInt(s[5]), 23, 59, 59);
				} 
				else {
					endDate.set(Calendar.YEAR, monthToInt(s[4]), Integer.parseInt(s[5]), 23, 59, 59);
				}
				
				// 1. Converts the request id to an int for the venue
				// 2. Calls a static function on room to convert the given list of rooms into a
				// 		explicit version used by the program
				// 3. The program then asks each venue to try make the reservation, if a reservation is
				// 		returned then we have a successful reservation and add it to the list.
				// 		otherwise the request is rejected
				for (Venue ven: venueList) {
					r = ven.makeBooking(startDate, endDate,
							Room.convertToList(Arrays.copyOfRange(s, 6, s.length)), Integer.parseInt(s[1]));
					if (r != null) break;
				}
				
				if (r == null) {
					System.out.println("Request rejected");
				}
				else {
					System.out.printf("Reservation %d %s %s\n", r.getId(), r.getOwnerVenue().getVenueName(), r.getRoomsString());
					reservationList.add(r);
				}
				
				break;
				
			case "Cancel":
				boolean foundID = false;
				r = getReservationByID(Integer.parseInt(s[1]));
				
				// Loop through the current reservations to find the res with the matching id
				// if it exists
				for (Reservation res: reservationList) {
					if (res.getId() == Integer.parseInt(s[1])) {
						foundID = true;
						break;
					}
				}
				
				// If an id is not found the cancellation is not performed
				if (!foundID) {
					System.out.println("Change rejected");
					break;
				}
				
				// If it is found the venue it belongs to is told to cancel it then
				// the reservation is removed also from the HireSystem's list
				v = r.getOwnerVenue();
				v.cancelBooking(r);
				
				reservationList.removeFirstOccurrence(r);
				System.out.printf("Cancel %s\n", s[1]);
				
				break;
				
			case "Change":
				startDate = new GregorianCalendar();
				endDate = new GregorianCalendar();	
				r = getReservationByID(s[1]);
				boolean successChange = false;
				
				// s[3] and s[5] are always valid according to the given spec
				startDate.set(Calendar.YEAR, monthToInt(s[2]), Integer.parseInt(s[3]), 0, 0, 0);
				
				// Checks if the new request lasts over a year
				if (isNextYear(s[2], s[3], s[4], s[5])) {
					endDate.set(Calendar.YEAR + 1, monthToInt(s[4]), Integer.parseInt(s[5]), 23, 59, 59);
				} 
				else {
					endDate.set(Calendar.YEAR, monthToInt(s[4]), Integer.parseInt(s[5]), 23, 59, 59);
				}
				
				// 1. Asks each venue weather or not they can handle the change,
				// 2. If the change is successful it is printed out otherwise
				// 		the original booking is kept
				for (Venue ven: venueList) {
					try {
						successChange = ven.changeBooking(r, startDate, endDate, 
								Room.convertToList(Arrays.copyOfRange(s, 6, s.length)));
					} catch (MissingReservationException e) {
						continue;		// if an exception is thrown the venue does not contain the reservation
										// Should not occur since the change booking method ensures this
					}
					
					if (successChange == true) break;
				}
				
				if (!successChange) {
					System.out.println("Change rejected");
				} 
				else {
					System.out.printf("Change %s %s %s\n", s[1], r.getOwnerVenue().getVenueName(), r.getRoomsString());
				}
				
				break;
				
			case "Print":
				// Calls the print function of the venue given
				getVenue(s[1]).printBookings();
				break;
				
			default:
				break;
		}
	}
	
	/**
	 * Given a ID number the method searches the list of reservations and returns
	 * 	the associated reservation if it exists
	 * @param i		ID of the reservation
	 * @return		Returns a reservation if one is found otherwise returns null
	 * @precondition	ID number is valid
	 * @postcondition	A valid reservation is returned
	 */
	private Reservation getReservationByID(int i) {
		
		for (Reservation r: reservationList) {
			if (r.getId() == i) {
				return r;
			}
		}
		
		return null;
	}
	
	/**
	 * Given a ID number the method searches the list of reservations and returns
	 * 	the associated reservation if it exists.
	 * This is an overload of {@link VenueHireSystem#getReservationByID(int)}
	 * to be used for a string ID, use the other method if possible
	 * 
	 * @param i		ID of the reservation
	 * @return		Returns a reservation if one is found otherwise returns null
	 * @precondition	ID number is valid
	 * @postcondition	A valid reservation is returned
	 */
	private Reservation getReservationByID(String s) {
		
		int i = Integer.parseInt(s);
		
		for (Reservation r: reservationList) {
			if (r.getId() == i) {
				return r;
			}
		}
		
		return null;
	}
	
	/**
	 * Given the name of a venue this will return the associated venue object
	 * 	if it exists
	 * @param name		String name of the venue
	 * @return			Returns a Venue object if found otherwise null
	 * @precondition	Name is valid
	 * @postcondition	Venue object is returned
	 */
	private Venue getVenue(String name) {
		for (Venue v: venueList) {
			if (v.getVenueName().equals(name)) return v;
		}
		return null;
	}
	
	/**
	 * Converts a string month into a number.
	 * Used for converting given months into a calendar acceptable format.
	 * As such the months are 0 indexed.
	 * @param month		3 Letter string of the month in question, 1st letter capitalized
	 * @return			Returns the 0 indexed number of the month
	 * @precondition 	Valid month
	 * @postcondition 	Valid digit
	 */
	private int monthToInt(String month) {
		switch(month) {
			case "Jan":
				return 0;
			case "Feb":
				return 1;
			case "Mar":
				return 2;
			case "Apr":
				return 3;
			case "May":
				return 4;
			case "Jun":
				return 5;
			case "Jul":
				return 6;
			case "Aug":
				return 7;
			case "Sep":
				return 8;
			case "Oct":
				return 9;
			case "Nov":
				return 10;
			default:
				return 11;
		}
	}
	
	/**
	 * Returns weather or not the 2nd date occurs in the next year
	 * @param month1		The starting month
	 * @param day1			The starting day
	 * @param month2		The ending month
	 * @param day2			The ending day
	 * @return				Boolean true if date 2 is in the next year, false otherwise
	 */
	private boolean isNextYear(String month1, String day1, String month2, String day2) {
		// Change strings to integers
		int adjustedMonth1 = monthToInt(month1);
		int adjustedMonth2 = monthToInt(month2);
		int adjustedDay1 = Integer.parseInt(day1);
		int adjustedDay2 = Integer.parseInt(day2);
		
		// If same month day2 must be less, else month2 must be less 
		if ((adjustedMonth2 < adjustedMonth1) || ((adjustedDay2 < adjustedDay1) && (adjustedMonth1 == adjustedMonth2)))
			return true;
		
		return false;
	}
}
