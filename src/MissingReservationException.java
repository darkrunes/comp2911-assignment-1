
/**
 * Exception to be thrown when a venue that should exist does not
 * @author Saffat Shams Akanda, z5061498
 */
public class MissingReservationException extends Exception {
	
	// Eclipse said I needed this
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor for exception, default message is null, see overload. 
	 * @param message		Message to be displayed
	 */
	public MissingReservationException(String message) {
		super(message);
	}
	
	/**
	 * Overload of constructor, takes no arguments; message = null
	 */
	public MissingReservationException() {
		super();
	}
}
